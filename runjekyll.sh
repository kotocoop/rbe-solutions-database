#!/bin/bash

docker run --rm --name "tzm-jekyll-builder-rbesolutions" --volume="$PWD:/srv/jekyll" -p "4002:4000" --volume="${HOME}/.bundle-cache/koto.rbesolutions:/usr/local/bundle" -it jekyll/jekyll:$JEKYLL_VERSION $*
